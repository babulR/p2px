angular.module( 'P2PX.singlerecord', [
    'ui.router',
    'plusOne'
  ])
  .config(function config( $stateProvider ) {
    $stateProvider.state( 'singlerecord', {
      url: '/singlerecord',
      views: {
        "main": {
          controller: 'SingleCtrl',
          templateUrl: 'singlerecord/singlerecord.tpl.html'
        }
      },
      data:{ pageTitle: 'Single Record' }
    });
  })
  .controller( 'SingleCtrl', function SingleController( $scope ) {
    
  })
  
  ;