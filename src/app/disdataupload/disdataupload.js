
angular.module( 'P2PX.disdataupload', [
    'ui.router',
    'plusOne'
  ])
  .config(function config( $stateProvider ) {
    $stateProvider.state( 'disdataupload', {
      url: '/disdataupload',
      views: {
        "main": {
          controller: 'DisdataCtrl',
          templateUrl: 'disdataupload/disdataupload.tpl.html'
        }
      },
      data:{ pageTitle: 'Disbursal Data' }
    });
  })
  .controller( 'DisdataCtrl', function DisbursalController( $scope,$http ) {
    $scope.uploadFile= function(){
        
      var file = $scope.myFile;
      var uploadUrl = 'http://localhost:5000/test';
      var fd = new FormData();
      fd.append('file', file);
      $http.post(uploadUrl, fd, {
          transformRequest: angular.identity,
          headers: {'Content-Type': undefined}
      })
      .success(function(response){
        if(response.success){
          $('#file').val('');
          $scope.myFile = '';
          $scope.errMsg = "Uploaded Successfully";
        }else{
          $('#file').val('');
          $scope.errMsg = response.message;
          $('#myModal').modal('show');
        }
      })
      .error(function(){
      });

  };
    $scope.filterOptions = {
      filterText: "",
      useExternalFilter: true
    };
    $scope.totalServerItems = 0;
    $scope.pagingOptions = {
        pageSizes: [5, 10, 15],
        pageSize: 5,
        currentPage: 1
    };
    $scope.setPagingData = function (data, page, pageSize) {
        var pagedData = data.data.slice((page - 1) * pageSize, page * pageSize);
        $scope.myData = pagedData;
        $scope.totalServerItems = data.data.length;
        if (!$scope.$$phase) {
            $scope.$apply();
        }
    };
    $scope.getPagedDataAsync = function (pageSize, page) {
        setTimeout(function () {
            $http.get('http://localhost:5000/getborrowerTransactions', {
            }).success(function (largeLoad) {
                $('#waitmsg').modal('hide');
                $scope.setPagingData(largeLoad, page, pageSize);
            });
        }, 100);
    };

    $scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage);

    $scope.$watch('filterOptions', function (newVal, oldVal) {
        if (newVal !== oldVal) {
            $scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage, $scope.filterOptions.filterText);
        }
    }, true);

    //$scope.mySelections = [];
    $scope.gridOptions = {
        //selectedItems: $scope.mySelections,
        data: 'myData',
        enablePaging: true,
        showFooter: true,
        //multiSelect: true,
        totalServerItems: 'totalServerItems',
        //showSelectionCheckbox: true,
        //plugins: [new NgGridFlexibleHeightPlugin()],
        columnDefs: [
          { field:"companyName",displayName: "companyName"},
          { field: "liveLoans",displayName: "liveLoans"},
          { field:"closedLoans",displayName: "closedLoans"},
          { field:"toatalOutStanding",displayName:"toatalOutStanding"}
        ]
        //pagingOptions: $scope.pagingOptions,
        //filterOptions: $scope.filterOptions
    };

  });
  
  