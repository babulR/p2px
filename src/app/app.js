angular.module( 'P2PX', [
  'ngGrid',
  'templates-app',
  'templates-common',
  'P2PX.home',
  'P2PX.about',
  'ui.router',
  'P2PX.uploaddocs',
  'P2PX.login',
  'P2PX.disdataupload',
  'P2PX.singlerecord',
  'P2PX.repayment',
  'P2PX.multirecords'
])

.config( function myAppConfig ( $stateProvider, $urlRouterProvider ,$httpProvider) {
  $urlRouterProvider.otherwise( '/home' );
  $httpProvider.defaults.headers.common['Access-Control-Allow-Origin'] = '*';
})

.run( function run () {
})

.controller( 'AppCtrl', function AppCtrl ( $scope, $location ) {
  $scope.$on('$stateChangeSuccess', function(event, toState, toParams, fromState, fromParams){
    if ( angular.isDefined( toState.data.pageTitle ) ) {
      $scope.pageTitle = toState.data.pageTitle + ' | P2PX' ;
    }
  });
}).directive('fileModel', ['$parse', function ($parse) {
  return {
      restrict: 'A',
      link: function(scope, element, attrs) {
          var model = $parse(attrs.fileModel);
          var modelSetter = model.assign;

          element.bind('change', function(){
              scope.$apply(function(){
                  modelSetter(scope, element[0].files[0]);
              });
          });
      }
  };
}]).service('fileUpload', ['$http', function ($http) {
  this.uploadFileToUrl = function(file, uploadUrl){
      var fd = new FormData();
      fd.append('file', file);
      $http.post(uploadUrl, fd, {
          transformRequest: angular.identity,
          headers: {'Content-Type': undefined}
      })
      .success(function(response){
        if(response.success){
          $scope.errMsg = "Uploaded Successfully";
        }else{
          errMsg = response.message;
          $('#myModal').show();
        }
      })
      .error(function(){
      });
  };
}]);

