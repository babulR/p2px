angular.module( 'P2PX.multirecords', [
    'ui.router',
    'plusOne'
  ])
  .config(function config( $stateProvider ) {
    $stateProvider.state( 'multirecords', {
      url: '/multirecords',
      views: {
        "main": {
          controller: 'MultirecCtrl',
          templateUrl: 'multiplerecords/multiplerecords.tpl.html'
        }
      },
      data:{ pageTitle: 'Multiple Records' }
    });
  })
  .controller( 'MultirecCtrl', function MultipleController( $scope ) {
    
  })
  
  ;