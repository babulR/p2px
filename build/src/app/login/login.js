
angular.module( 'P2PX.login', [
    'ui.router',
    'plusOne'
  ])
  .config(function config( $stateProvider ) {
    $stateProvider.state( 'login', {
      url: '/login',
      views: {
        "main": {
          controller: 'LoginCtrl',
          templateUrl: 'login/login.tpl.html'
        }
      },
      data:{ pageTitle: 'Login' }
    });
  })
  .controller( 'LoginCtrl', function LoginController( $scope ) {
    
  })
  
  ;
  
  