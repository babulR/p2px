/**
 * Each section of the site has its own module. It probably also has
 * submodules, though this boilerplate is too simple to demonstrate it. Within
 * `src/app/home`, however, could exist several additional folders representing
 * additional modules that would then be listed as dependencies of this one.
 * For example, a `note` section could have the submodules `note.create`,
 * `note.delete`, `note.edit`, etc.
 *
 * Regardless, so long as dependencies are managed correctly, the build process
 * will automatically take take of the rest.
 *
 * The dependencies block here is also where component dependencies should be
 * specified, as shown below.
 */
angular.module( 'P2PX.uploaddocs', [
    'ngGrid',
    'ui.router',
    'plusOne'
  ])
  
  /**
   * Each section or module of the site can also have its own routes. AngularJS
   * will handle ensuring they are all available at run-time, but splitting it
   * this way makes each module more "self-contained".
   */
  .config(function config( $stateProvider ) {
    $stateProvider.state( 'uploaddocs', {
      url: '/uploaddocs',
      views: {
        "main": {
          controller: 'DocsCtrl',
          templateUrl: 'uploaddocs/uploaddocs.tpl.html'
        }
      },
      data:{ pageTitle: 'Upload Docs' }
    });
  })
  
  /**
   * And of course we define a controller for our route.
   */
  .controller( 'DocsCtrl', function DocsController( $scope,$http ) {
    $scope.uploadFile= function(){
        
        var file = $scope.myFile;
        var uploadUrl = 'http://localhost:5000/test';
        var fd = new FormData();
        fd.append('file', file);
        $http.post(uploadUrl, fd, {
            transformRequest: angular.identity,
            headers: {'Content-Type': undefined}
        })
        .success(function(response){
          if(response.success){
            $('#file').val('');
            $scope.myFile = '';
            $scope.errMsg = "Uploaded Successfully";
          }else{
            $('#file').val('');
            $scope.errMsg = response.message;
            $('#myModal').modal('show');
          }
        })
        .error(function(){
        });

    };
    $scope.downloadSample = function(){
      var sampleUrl = 'http://localhost:5000/downloadSample2';
      $http.get(sampleUrl, {
        transformRequest: angular.identity,
        headers: {'Content-Type': undefined}
    });
  };

  
  grid = true;
  $scope.filterOptions = {
    filterText: "",
    useExternalFilter: true
};
$scope.totalServerItems = 0;
$scope.pagingOptions = {
    pageSizes: [5, 10, 15],
    pageSize: 5,
    currentPage: 1
};
$scope.setPagingData = function (data, page, pageSize) {
    var pagedData = data.data.slice((page - 1) * pageSize, page * pageSize);
    $scope.myData = pagedData;
    $scope.totalServerItems = data.data.length;
    if (!$scope.$$phase) {
        $scope.$apply();
    }
};
$scope.getPagedDataAsync = function (pageSize, page) {
    setTimeout(function () {
        $http.get('http://localhost:5000/getborrowerTransactions', {
        }).success(function (largeLoad) {
            $('#waitmsg').modal('hide');
            $scope.setPagingData(largeLoad, page, pageSize);
        });
    }, 100);
};

$scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage);

$scope.$watch('filterOptions', function (newVal, oldVal) {
    if (newVal !== oldVal) {
        $scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage, $scope.filterOptions.filterText);
    }
}, true);

//$scope.mySelections = [];
$scope.gridOptions = {
    //selectedItems: $scope.mySelections,
    data: 'myData',
    enablePaging: true,
    showFooter: true,
    //multiSelect: true,
    totalServerItems: 'totalServerItems',
    //showSelectionCheckbox: true,
    //plugins: [new NgGridFlexibleHeightPlugin()],
    columnDefs: [
      { companyName: "Organization", width: 120, pinned: true },
      { liveLoans: "liveLoans", width: 120 },
      { closedLoans: "closedLoans", width: 120 },
      { liveLoanAmount: "liveLoanAmount", width: 120 }
    ],
    pagingOptions: $scope.pagingOptions,
    filterOptions: $scope.filterOptions
};


});
  
  