angular.module( 'P2PX.repayment', [
    'ui.router',
    'plusOne'
  ])
  .config(function config( $stateProvider ) {
    $stateProvider.state( 'repayment', {
      url: '/repayment',
      views: {
        "main": {
          controller: 'RepaymentCtrl',
          templateUrl: 'repaymentdata/repayment.tpl.html'
        }
      },
      data:{ pageTitle: 'Repayment' }
    });
  })
  .controller( 'RepaymentCtrl', function RepaymentController( $scope ) {
    
  })
  
  ;