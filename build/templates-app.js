angular.module('templates-app', ['about/about.tpl.html', 'disdataupload/disdataupload.tpl.html', 'home/home.tpl.html', 'login/login.tpl.html', 'multiplerecords/multiplerecords.tpl.html', 'repaymentdata/repayment.tpl.html', 'siglerecord/singlerecord.tpl.html', 'singlerecord/singlerecord.tpl.html', 'uploaddocs/uploaddocs.tpl.html']);

angular.module("about/about.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("about/about.tpl.html",
    "<div class=\"row\">\n" +
    "  <h1 class=\"page-header\">\n" +
    "    The Elevator Pitch\n" +
    "    <small>For the lazy and impatient.</small>\n" +
    "  </h1>\n" +
    "  <p>\n" +
    "    <code>ng-boilerplate</code> is an opinionated kickstarter for web\n" +
    "    development projects. It's an attempt to create a simple starter for new\n" +
    "    web sites and apps: just download it and start coding. The goal is to\n" +
    "    have everything you need to get started out of the box; of course it has\n" +
    "    slick styles and icons, but it also has a best practice directory structure\n" +
    "    to ensure maximum code reuse. And it's all tied together with a robust\n" +
    "    build system chock-full of some time-saving goodness.\n" +
    "  </p>\n" +
    "\n" +
    "  <h2>Why?</h2>\n" +
    "\n" +
    "  <p>\n" +
    "    Because my team and I make web apps, and \n" +
    "    last year AngularJS became our client-side framework of choice. We start\n" +
    "    websites the same way every time: create a directory structure, copy and\n" +
    "    ever-so-slightly tweak some config files from an older project, and yada\n" +
    "    yada, etc., and so on and so forth. Why are we repeating ourselves? We wanted a starting point; a set of\n" +
    "    best practices that we could identify our projects as embodying and a set of\n" +
    "    time-saving wonderfulness, because time is money.\n" +
    "  </p>\n" +
    "\n" +
    "  <p>\n" +
    "    There are other similar projects out there, but none of them suited our\n" +
    "    needs. Some are awesome but were just too much, existing more as reference\n" +
    "    implementations, when we really just wanted a kickstarter. Others were just\n" +
    "    too little, with puny build systems and unscalable architectures.  So we\n" +
    "    designed <code>ng-boilerplate</code> to be just right.\n" +
    "  </p>\n" +
    "\n" +
    "  <h2>What ng-boilerplate Is Not</h2>\n" +
    "\n" +
    "  <p>\n" +
    "    This is not an example of an AngularJS app. This is a kickstarter. If\n" +
    "    you're looking for an example of what a complete, non-trivial AngularJS app\n" +
    "    that does something real looks like, complete with a REST backend and\n" +
    "    authentication and authorization, then take a look at <code><a\n" +
    "        href=\"https://github.com/angular-app/angular-app/\">angular-app</a></code>, \n" +
    "    which does just that, and does it well.\n" +
    "  </p>\n" +
    "\n" +
    "  <h1 class=\"page-header\">\n" +
    "    So What's Included?\n" +
    "    <small>I'll try to be more specific than \"awesomeness\".</small>\n" +
    "  </h1>\n" +
    "  <p>\n" +
    "    This section is just a quick introduction to all the junk that comes\n" +
    "    pre-packaged with <code>ng-boilerplate</code>. For information on how to\n" +
    "    use it, see the <a\n" +
    "      href=\"https://github.com/joshdmiller/ng-boilerplate#readme\">project page</a> at\n" +
    "    GitHub.\n" +
    "  </p>\n" +
    "\n" +
    "  <p>\n" +
    "    The high-altitude view is that the base project includes \n" +
    "    <a href=\"http://getbootstrap.com\">Twitter Bootstrap</a>\n" +
    "    styles to quickly produce slick-looking responsive web sites and apps. It also\n" +
    "    includes <a href=\"http://angular-ui.github.com/bootstrap\">UI Bootstrap</a>,\n" +
    "    a collection of native AngularJS directives based on the aforementioned\n" +
    "    templates and styles. It also includes <a href=\"http://fortawesome.github.com/Font-Awesome/\">Font Awesome</a>,\n" +
    "    a wicked-cool collection of font-based icons that work swimmingly with all\n" +
    "    manner of web projects; in fact, all images on the site are actually font-\n" +
    "    based icons from Font Awesome. Neat! Lastly, this also includes\n" +
    "    <a href=\"http://joshdmiller.github.com/angular-placeholders\">Angular Placeholders</a>,\n" +
    "    a set of pure AngularJS directives to do client-side placeholder images and\n" +
    "    text to make mocking user interfaces super easy.\n" +
    "  </p>\n" +
    "\n" +
    "  <p>\n" +
    "    And, of course, <code>ng-boilerplate</code> is built on <a href=\"http://angularjs.org\">AngularJS</a>,\n" +
    "    by the far the best JavaScript framework out there! But if you don't know\n" +
    "    that already, then how did you get here? Well, no matter - just drink the\n" +
    "    Kool Aid. Do it. You know you want to.\n" +
    "  </p>\n" +
    "  \n" +
    "  <h2>Twitter Bootstrap</h2>\n" +
    "\n" +
    "  <p>\n" +
    "    You already know about this, right? If not, <a\n" +
    "      href=\"http://getbootstrap.com\">hop on over</a> and check it out; it's\n" +
    "    pretty sweet. Anyway, all that wonderful stylistic goodness comes built in.\n" +
    "    The LESS files are available for you to import in your main stylesheet as\n" +
    "    needed - no excess, no waste. There is also a dedicated place to override\n" +
    "    variables and mixins to suit your specific needs, so updating to the latest\n" +
    "    version of Bootstrap is as simple as: \n" +
    "  </p>\n" +
    "\n" +
    "  <pre>$ cd vendor/twitter-bootstrap<br />$ git pull origin master</pre>\n" +
    "\n" +
    "  <p>\n" +
    "    Boom! And victory is ours.\n" +
    "  </p>\n" +
    "\n" +
    "  <h2>UI Bootstrap</h2>\n" +
    "\n" +
    "  <p>\n" +
    "    What's better than Bootstrap styles? Bootstrap directives!  The fantastic <a href=\"http://angular-ui.github.com/bootstrap\">UI Bootstrap</a>\n" +
    "    library contains a set of native AngularJS directives that are endlessly\n" +
    "    extensible. You get the tabs, the tooltips, the accordions. You get your\n" +
    "    carousel, your modals, your pagination. And <i>more</i>.\n" +
    "    How about a quick demo? \n" +
    "  </p>\n" +
    "\n" +
    "  <ul>\n" +
    "    <li class=\"dropdown\">\n" +
    "      <a class=\"btn dropdown-toggle\">\n" +
    "        Click me!\n" +
    "      </a>\n" +
    "      <ul class=\"dropdown-menu\">\n" +
    "        <li ng-repeat=\"choice in dropdownDemoItems\">\n" +
    "          <a>{{choice}}</a>\n" +
    "        </li>\n" +
    "      </ul>\n" +
    "    </li>\n" +
    "  </ul>\n" +
    "\n" +
    "  <p>\n" +
    "    Oh, and don't include jQuery;  \n" +
    "    you don't need it.\n" +
    "    This is better.\n" +
    "    Don't be one of those people. <sup>*</sup>\n" +
    "  </p>\n" +
    "\n" +
    "  <p><small><sup>*</sup> Yes, there are exceptions.</small></p>\n" +
    "\n" +
    "  <h2>Font Awesome</h2>\n" +
    "\n" +
    "  <p>\n" +
    "    Font Awesome has earned its label. It's a set of open (!) icons that come\n" +
    "    distributed as a font (!) for super-easy, lightweight use. Font Awesome \n" +
    "    works really well with Twitter Bootstrap, and so fits right in here.\n" +
    "  </p>\n" +
    "\n" +
    "  <p>\n" +
    "    There is not a single image on this site. All of the little images and icons \n" +
    "    are drawn through Font Awesome! All it takes is a little class:\n" +
    "  </p>\n" +
    "\n" +
    "  <pre>&lt;i class=\"fa fa-flag\"&gt;&lt/i&gt</pre>\n" +
    "\n" +
    "  <p>\n" +
    "    And you get one of these: <i class=\"fa fa-flag\"> </i>. Neat!\n" +
    "  </p>\n" +
    "\n" +
    "  <h2>Placeholders</h2>\n" +
    "\n" +
    "  <p>\n" +
    "    Angular Placeholders is a simple library for mocking up text and images. You\n" +
    "    can automatically throw around some \"lorem ipsum\" text:\n" +
    "  </p>\n" +
    "\n" +
    "  <pre>&lt;p ph-txt=\"3s\"&gt;&lt;/p&gt;</pre>\n" +
    "\n" +
    "  <p>\n" +
    "    Which gives you this:\n" +
    "  </p>\n" +
    "\n" +
    "  <div class=\"pre\">\n" +
    "    &lt;p&gt;\n" +
    "    <p ph-txt=\"3s\"></p>\n" +
    "    &lt;/p&gt;\n" +
    "  </div>\n" +
    "\n" +
    "  <p>\n" +
    "    Even more exciting, you can also create placeholder images, entirely \n" +
    "    client-side! For example, this:\n" +
    "  </p>\n" +
    "  \n" +
    "  <pre>\n" +
    "&lt;img ph-img=\"50x50\" /&gt;\n" +
    "&lt;img ph-img=\"50x50\" class=\"img-polaroid\" /&gt;\n" +
    "&lt;img ph-img=\"50x50\" class=\"img-rounded\" /&gt;\n" +
    "&lt;img ph-img=\"50x50\" class=\"img-circle\" /&gt;</pre>\n" +
    "\n" +
    "  <p>\n" +
    "    Gives you this:\n" +
    "  </p>\n" +
    "\n" +
    "  <div>\n" +
    "    <img ph-img=\"50x50\" />\n" +
    "    <img ph-img=\"50x50\" class=\"img-polaroid\" />\n" +
    "    <img ph-img=\"50x50\" class=\"img-rounded\" />\n" +
    "    <img ph-img=\"50x50\" class=\"img-circle\" />\n" +
    "  </div>\n" +
    "\n" +
    "  <p>\n" +
    "    Which is awesome.\n" +
    "  </p>\n" +
    "\n" +
    "  <h1 class=\"page-header\">\n" +
    "    The Roadmap\n" +
    "    <small>Really? What more could you want?</small>\n" +
    "  </h1>\n" +
    "\n" +
    "  <p>\n" +
    "    This is a project that is <i>not</i> broad in scope, so there's not really\n" +
    "    much of a wish list here. But I would like to see a couple of things:\n" +
    "  </p>\n" +
    "\n" +
    "  <p>\n" +
    "    I'd like it to be a little simpler. I want this to be a universal starting\n" +
    "    point. If someone is starting a new AngularJS project, she should be able to\n" +
    "    clone, merge, or download its source and immediately start doing what she\n" +
    "    needs without renaming a bunch of files and methods or deleting spare parts\n" +
    "    ... like this page. This works for a first release, but I just think there\n" +
    "    is a little too much here right now.\n" +
    "  </p>\n" +
    "\n" +
    "  <p>\n" +
    "    I'd also like to see a simple generator. Nothing like <a href=\"yeoman.io\">\n" +
    "    Yeoman</a>, as there already is one of those, but just something that\n" +
    "    says \"I want Bootstrap but not Font Awesome and my app is called 'coolApp'.\n" +
    "    Gimme.\" Perhaps a custom download builder like UI Bootstrap\n" +
    "    has. Like that. Then again, perhaps some Yeoman generators wouldn't be out\n" +
    "    of line. I don't know. What do you think?\n" +
    "  </p>\n" +
    "\n" +
    "  <p>\n" +
    "    Naturally, I am open to all manner of ideas and suggestions. See the \"Can I\n" +
    "    Help?\" section below.\n" +
    "  </p>\n" +
    "\n" +
    "  <h2>The Tactical To Do List</h2>\n" +
    "\n" +
    "  <p>\n" +
    "    There isn't much of a demonstration of UI Bootstrap. I don't want to pollute\n" +
    "    the code with a demo for demo's sake, but I feel we should showcase it in\n" +
    "    <i>some</i> way.\n" +
    "  </p>\n" +
    "\n" +
    "  <p>\n" +
    "    <code>ng-boilerplate</code> should include end-to-end tests. This should\n" +
    "    happen soon. I just haven't had the time.\n" +
    "  </p>\n" +
    "\n" +
    "  <p>\n" +
    "    Lastly, this site should be prettier, but I'm no web designer. Throw me a\n" +
    "    bone here, people!\n" +
    "  </p>\n" +
    "\n" +
    "  <h2>Can I Help?</h2>\n" +
    "\n" +
    "  <p>\n" +
    "    Yes, please!\n" +
    "  </p>\n" +
    "\n" +
    "  <p>\n" +
    "    This is an opinionated kickstarter, but the opinions are fluid and\n" +
    "    evidence-based. Don't like the way I did something? Think you know of a\n" +
    "    better way? Have an idea to make this more useful? Let me know! You can\n" +
    "    contact me through all the usual channels or you can open an issue on the\n" +
    "    GitHub page. If you're feeling ambitious, you can even submit a pull\n" +
    "    request - how thoughtful of you!\n" +
    "  </p>\n" +
    "\n" +
    "  <p>\n" +
    "    So join the team! We're good people.\n" +
    "  </p>\n" +
    "</div>\n" +
    "\n" +
    "");
}]);

angular.module("disdataupload/disdataupload.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("disdataupload/disdataupload.tpl.html",
    "<div class=\"row half-vertical-gutters align-middle\">\n" +
    "        <!-- <div class=\"column shrink\"><a id=\"mnav\"><i class=\"icon reorder\"></i></a></div> -->\n" +
    "        <div class=\"column\">\n" +
    "            <div class=\"wojo small breadcrumb\">\n" +
    "                <div class=\"active section\">Data Upload</div>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "        <div class=\"column shrink padding-right\">\n" +
    "            <div class=\"wojo top right pointing dropdown\" tabindex=\"0\"><img src=\"assets/images/blank.png\" alt=\"\" class=\"wojo basic tiny circular image\">\n" +
    "                <div class=\"menu\" tabindex=\"-1\"> <a class=\"item\" href=\"\">\n" +
    "                        <!-- <i class=\"icon note\"></i> -->\n" +
    "                        <i class=\"fa fa-pencil-square-o\" aria-hidden=\"true\"></i> My Account</a>\n" +
    "                    <a class=\"item\" href=\"\">\n" +
    "                        <!-- <i class=\"icon lock\"></i> -->\n" +
    "                        <i class=\"fa fa-unlock-alt\" aria-hidden=\"true\"></i> Change Password</a> </div>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "        <div class=\"column shrink padding-right\">\n" +
    "            <div class=\"wojo top right pointing dropdown\" tabindex=\"0\">\n" +
    "                <i class=\"fa fa-bars\" aria-hidden=\"true\"></i>\n" +
    "                <div class=\"menu\" tabindex=\"-1\"> <a class=\"item\" href=\"\">Permissions</a>\n" +
    "                    <a class=\"item\" href=\"\">Payment Options</a>\n" +
    "                    <a class=\"item\" href=\"\">Transactions</a>\n" +
    "                    <a class=\"item\" href=\"\">System</a>\n" +
    "                    <a class=\"item\" href=\"\">Trash</a>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "        <div class=\"column shrink\"><a href=\"\">\n" +
    "                <i class=\"fa fa-power-off\" aria-hidden=\"true\"></i></a></div>\n" +
    "</div>\n" +
    "<h3>Disbursal</h3>\n" +
    "<div class=\"wojo segment\">\n" +
    "    <div class=\"row gutters\">\n" +
    "        <div class=\"column \">\n" +
    "            <div class=\"upload\">\n" +
    "                <label for=\"file-upload\" class=\"custom-file-upload\">\n" +
    "                    <img src=\"assets/images/upload.png\" /><br />Upload a file\n" +
    "                </label>\n" +
    "                <input id=\"file-upload\" type=\"file\" file-model=\"myFile\"/>\n" +
    "            </div>\n" +
    "            <div class=\"download\">\n" +
    "                <a href=\"\" class=\"custom-file-download\">\n" +
    "                    <img src=\"assets/images/download.png\" /><br />Sample csv file\n" +
    "                </a>\n" +
    "            </div>\n" +
    "\n" +
    "        </div>\n" +
    "    </div>\n" +
    "    \n" +
    "    <div class=\"fileupload col-lg-6 col-md-6 col-sm-6\">\n" +
    "        <span>{{myFile.name}}</span>\n" +
    "        <input ng-if = \"myFile\" type=\"button\" ng-click=\"uploadFile()\" value = \"Upload\"/>\n" +
    "    </div>\n" +
    "\n" +
    "    \n" +
    "\n" +
    "</div>\n" +
    "<div class=\"row\">\n" +
    "    <div class=\"gridStyle\" ng-grid=\"gridOptions\"></div>\n" +
    "</div>");
}]);

angular.module("home/home.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("home/home.tpl.html",
    "\n" +
    "<div class=\"row half-vertical-gutters align-middle\">\n" +
    "        <!-- <div class=\"column shrink\"><a id=\"mnav\"><i class=\"icon reorder\"></i></a></div> -->\n" +
    "        <div class=\"column\">\n" +
    "          <div class=\"wojo small breadcrumb\"> <div class=\"active section\">Home</div> </div>\n" +
    "        </div>\n" +
    "        <div class=\"column shrink padding-right\">\n" +
    "          <div class=\"wojo top right pointing dropdown\" tabindex=\"0\"><img src=\"assets/images/blank.png\" alt=\"\" class=\"wojo basic tiny circular image\">\n" +
    "            <div class=\"menu\" tabindex=\"-1\"> <a class=\"item\" href=\"\"><!-- <i class=\"icon note\"></i> --> \n" +
    "            <i class=\"fa fa-pencil-square-o\" aria-hidden=\"true\"></i> My Account</a> \n" +
    "            <a class=\"item\" href=\"\"> <!-- <i class=\"icon lock\"></i> -->\n" +
    "            <i class=\"fa fa-unlock-alt\" aria-hidden=\"true\"></i> Change Password</a> </div>\n" +
    "          </div>\n" +
    "        </div>\n" +
    "          <div class=\"column shrink padding-right\">\n" +
    "          <div class=\"wojo top right pointing dropdown\" tabindex=\"0\">\n" +
    "            <!-- <i class=\"icon black apps link\"></i> --> <i class=\"fa fa-bars\" aria-hidden=\"true\"></i>\n" +
    "            <div class=\"menu\" tabindex=\"-1\"> <a class=\"item\" href=\"\">Permissions</a> \n" +
    "            <a class=\"item\" href=\"\">Payment Options</a> \n" +
    "            <a class=\"item\" href=\"\">Transactions</a>\n" +
    "            <a class=\"item\" href=\"\">System</a>\n" +
    "            <a class=\"item\" href=\"\">Trash</a>\n" +
    "            </div>\n" +
    "          </div>\n" +
    "        </div>\n" +
    "          <div class=\"column shrink\"><a href=\"\">\n" +
    "          <!-- <i class=\"icon black power link\"></i> --><i class=\"fa fa-power-off\" aria-hidden=\"true\"></i></a></div>\n" +
    "</div><h3>Welcome to dashboard</h3>\n" +
    "<div class=\"wojo segment\">\n" +
    "        <div class=\"row gutters\">\n" +
    "          <div class=\"column screen-25 tablet-50 mobile-50 phone-100\">\n" +
    "            <div class=\"wojo card\">\n" +
    "              <div class=\"content content-center\"><span class=\"wojo positive basic circular gigantic label\">18</span></div>\n" +
    "              <div class=\"foot content-center wojo white text\">Total Users</div>\n" +
    "            </div>\n" +
    "          </div>\n" +
    "          <div class=\"column screen-25 tablet-50 mobile-50 phone-100\">\n" +
    "            <div class=\"wojo card\">\n" +
    "              <div class=\"content content-center\"><span class=\"wojo secondary basic circular gigantic label\">14</span></div>\n" +
    "              <div class=\"foot content-center wojo white text\">Successful</div>\n" +
    "            </div>\n" +
    "          </div>\n" +
    "          <div class=\"column screen-25 tablet-50 mobile-50 phone-100\">\n" +
    "            <div class=\"wojo card\">\n" +
    "              <div class=\"content content-center\"><span class=\"wojo negative basic circular gigantic label\">1</span></div>\n" +
    "              <div class=\"foot content-center wojo white text\">Unsuccessful</div>\n" +
    "            </div>\n" +
    "          </div>\n" +
    "          <div class=\"column screen-25 tablet-50 mobile-50 phone-100\">\n" +
    "            <div class=\"wojo card\">\n" +
    "              <div class=\"content content-center\"><span class=\"wojo primary basic circular gigantic label\">16</span></div>\n" +
    "              <div class=\"foot content-center wojo white text\">Tickets</div>\n" +
    "            </div>\n" +
    "          </div>\n" +
    "        </div>\n" +
    "          <h4>Total collected all time</h4>\n" +
    "    \n" +
    "      \n" +
    "        <h4>Sales By Membership</h4>\n" +
    "        <div id=\"legend2\" class=\"wojo small horizontal list align-right\"><div class=\"item\"><span class=\"wojo tiny empty circular label\" style=\"background:#2196f3\"> </span> Trial</div><div class=\"item\"><span class=\"wojo tiny empty circular label\" style=\"background:#e91e63\"> </span> Bronze</div><div class=\"item\"><span class=\"wojo tiny empty circular label\" style=\"background:#4caf50\"> </span> Gold</div><div class=\"item\"><span class=\"wojo tiny empty circular label\" style=\"background:#ff9800\"> </span> Platinum</div></div>\n" +
    "      \n" +
    "        \n" +
    "</div>");
}]);

angular.module("login/login.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("login/login.tpl.html",
    "<style>\n" +
    "       \n" +
    "       .container-fluid{\n" +
    "            display :none;\n" +
    "        }\n" +
    "        .sidebar{\n" +
    "            display :none;\n" +
    "        }\n" +
    "      body {font-family: Arial, Helvetica, sans-serif;background-color: #ffff;}\n" +
    "      .loginarea form {border: 3px solid #f1f1f1;}\n" +
    "      \n" +
    "      .loginarea input[type=text], input[type=password] {\n" +
    "          width: 100%;\n" +
    "          padding: 12px 20px;\n" +
    "          margin: 8px 0;\n" +
    "          display: inline-block;\n" +
    "          border: 1px solid #ccc;\n" +
    "          box-sizing: border-box;\n" +
    "      }\n" +
    "      \n" +
    "      .loginarea button {\n" +
    "          background-color: #4CAF50;\n" +
    "          color: white;\n" +
    "          padding: 14px 20px;\n" +
    "          margin: 8px 0;\n" +
    "          border: none;\n" +
    "          cursor: pointer;\n" +
    "          width: 100%;\n" +
    "      }\n" +
    "      \n" +
    "      .loginarea button:hover {\n" +
    "          opacity: 0.8;\n" +
    "      }\n" +
    "      \n" +
    "      .loginarea .cancelbtn {\n" +
    "          width: auto;\n" +
    "          padding: 10px 18px;\n" +
    "          background-color: #f44336;\n" +
    "      }\n" +
    "      \n" +
    "      .loginarea .imgcontainer {\n" +
    "          text-align: center;\n" +
    "          margin: 24px 0 12px 0;\n" +
    "      }\n" +
    "      \n" +
    "      .loginarea img.avatar {\n" +
    "          width: 10%;\n" +
    "          border-radius: 50%;\n" +
    "      }\n" +
    "      \n" +
    "      .loginarea .container {\n" +
    "          padding: 16px;\n" +
    "      }\n" +
    "      \n" +
    "      .loginarea span.psw {\n" +
    "          float: right;\n" +
    "          padding-top: 16px;\n" +
    "      }\n" +
    "      \n" +
    "      /* Change styles for span and cancel button on extra small screens */\n" +
    "      @media screen and (max-width: 300px) {\n" +
    "        .loginarea span.psw {\n" +
    "             display: block;\n" +
    "             float: none;\n" +
    "          }\n" +
    "          .loginarea .cancelbtn {\n" +
    "             width: 100%;\n" +
    "          }\n" +
    "      }\n" +
    "    \n" +
    "      .loginarea{width: 500px; margin: auto;}\n" +
    "    </style>\n" +
    "    <div class=\"loginarea\">  \n" +
    "    <h2 class=\"text-center\">Login Form</h2>\n" +
    "      \n" +
    "      <form action=\"/action_page.php\">\n" +
    "        \n" +
    "      \n" +
    "        <div class=\"container\" style = \"width: 100%;\">\n" +
    "          <label for=\"uname\"><b>Username</b></label>\n" +
    "          <input type=\"text\" placeholder=\"Enter Username\" name=\"uname\" required>\n" +
    "      \n" +
    "          <label for=\"psw\"><b>Password</b></label>\n" +
    "          <input type=\"password\" placeholder=\"Enter Password\" name=\"psw\" required>\n" +
    "              \n" +
    "          <button type=\"submit\">Login</button>\n" +
    "          <label>\n" +
    "            <input type=\"checkbox\" checked=\"checked\" name=\"remember\"> Remember me\n" +
    "          </label>\n" +
    "        </div>\n" +
    "      \n" +
    "        <div class=\"container\" style=\"background-color:#f1f1f1; width: 100%;\">\n" +
    "          <span class=\"psw\">Forgot <a href=\"#\">password?</a></span>\n" +
    "        </div>\n" +
    "      </form>\n" +
    "    </div>");
}]);

angular.module("multiplerecords/multiplerecords.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("multiplerecords/multiplerecords.tpl.html",
    "\n" +
    "<div class=\"row half-vertical-gutters align-middle\">\n" +
    "  <!-- <div class=\"column shrink\"><a id=\"mnav\"><i class=\"icon reorder\"></i></a></div> -->\n" +
    "  <div class=\"column\">\n" +
    "    <div class=\"wojo small breadcrumb\"> <div class=\"active section\">Data Upload</div> </div>\n" +
    "  </div>\n" +
    "  <div class=\"column shrink padding-right\">\n" +
    "    <div class=\"wojo top right pointing dropdown\" tabindex=\"0\"><img src=\"assets/images/blank.png\" alt=\"\" class=\"wojo basic tiny circular image\">\n" +
    "      <div class=\"menu\" tabindex=\"-1\"> <a class=\"item\" href=\"\"><!-- <i class=\"icon note\"></i> --> \n" +
    "      <i class=\"fa fa-pencil-square-o\" aria-hidden=\"true\"></i> My Account</a> \n" +
    "      <a class=\"item\" href=\"\"> <!-- <i class=\"icon lock\"></i> -->\n" +
    "      <i class=\"fa fa-unlock-alt\" aria-hidden=\"true\"></i> Change Password</a> </div>\n" +
    "    </div>\n" +
    "  </div>\n" +
    "    <div class=\"column shrink padding-right\">\n" +
    "    <div class=\"wojo top right pointing dropdown\" tabindex=\"0\">\n" +
    "      <i class=\"fa fa-bars\" aria-hidden=\"true\"></i>\n" +
    "      <div class=\"menu\" tabindex=\"-1\"> <a class=\"item\" href=\"\">Permissions</a> \n" +
    "      <a class=\"item\" href=\"\">Payment Options</a> \n" +
    "      <a class=\"item\" href=\"\">Transactions</a>\n" +
    "      <a class=\"item\" href=\"\">System</a>\n" +
    "      <a class=\"item\" href=\"\">Trash</a>\n" +
    "      </div>\n" +
    "    </div>\n" +
    "  </div>\n" +
    "    <div class=\"column shrink\"><a href=\"\">\n" +
    "    <i class=\"fa fa-power-off\" aria-hidden=\"true\"></i></a></div>\n" +
    "</div><h3>Multiple Records</h3>\n" +
    "<div class=\"wojo segment\">\n" +
    "  <div class=\"row gutters\">\n" +
    "    <div class=\"column \">\n" +
    "      <div class=\"upload\">\n" +
    "          <label for=\"file-upload\" class=\"custom-file-upload\">\n" +
    "            <img src=\"assets/images/upload.png\"/><br/>Upload csv file\n" +
    "          </label>\n" +
    "          <input id=\"file-upload\" type=\"file\"/>\n" +
    "      </div>\n" +
    "      <div class=\"download\">\n" +
    "        <a href=\"\" class=\"custom-file-download\">\n" +
    "          <img src=\"assets/images/download.png\"/><br/>Sample csv file\n" +
    "        </a>\n" +
    "      </div>\n" +
    "        \n" +
    "    </div>\n" +
    "  </div>\n" +
    "  \n" +
    "  </div>\n" +
    "");
}]);

angular.module("repaymentdata/repayment.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("repaymentdata/repayment.tpl.html",
    "\n" +
    "<div class=\"row half-vertical-gutters align-middle\">\n" +
    "  <!-- <div class=\"column shrink\"><a id=\"mnav\"><i class=\"icon reorder\"></i></a></div> -->\n" +
    "  <div class=\"column\">\n" +
    "    <div class=\"wojo small breadcrumb\"> <div class=\"active section\">Data Upload</div> </div>\n" +
    "  </div>\n" +
    "  <div class=\"column shrink padding-right\">\n" +
    "    <div class=\"wojo top right pointing dropdown\" tabindex=\"0\"><img src=\"assets/images/blank.png\" alt=\"\" class=\"wojo basic tiny circular image\">\n" +
    "      <div class=\"menu\" tabindex=\"-1\"> <a class=\"item\" href=\"\"><!-- <i class=\"icon note\"></i> --> \n" +
    "      <i class=\"fa fa-pencil-square-o\" aria-hidden=\"true\"></i> My Account</a> \n" +
    "      <a class=\"item\" href=\"\"> <!-- <i class=\"icon lock\"></i> -->\n" +
    "      <i class=\"fa fa-unlock-alt\" aria-hidden=\"true\"></i> Change Password</a> </div>\n" +
    "    </div>\n" +
    "  </div>\n" +
    "    <div class=\"column shrink padding-right\">\n" +
    "    <div class=\"wojo top right pointing dropdown\" tabindex=\"0\">\n" +
    "      <i class=\"fa fa-bars\" aria-hidden=\"true\"></i>\n" +
    "      <div class=\"menu\" tabindex=\"-1\"> <a class=\"item\" href=\"\">Permissions</a> \n" +
    "      <a class=\"item\" href=\"\">Payment Options</a> \n" +
    "      <a class=\"item\" href=\"\">Transactions</a>\n" +
    "      <a class=\"item\" href=\"\">System</a>\n" +
    "      <a class=\"item\" href=\"\">Trash</a>\n" +
    "      </div>\n" +
    "    </div>\n" +
    "  </div>\n" +
    "    <div class=\"column shrink\"><a href=\"\">\n" +
    "    <i class=\"fa fa-power-off\" aria-hidden=\"true\"></i></a></div>\n" +
    "</div><h3>Repayment</h3>\n" +
    "<div class=\"wojo segment\">\n" +
    "  <div class=\"row gutters\">\n" +
    "    <div class=\"column \">\n" +
    "      <div class=\"upload\">\n" +
    "          <label for=\"file-upload\" class=\"custom-file-upload\">\n" +
    "            <img src=\"assets/images/upload.png\"/><br/>Upload csv file\n" +
    "          </label>\n" +
    "          <input id=\"file-upload\" type=\"file\"/>\n" +
    "      </div>\n" +
    "      <div class=\"download\">\n" +
    "        <a href=\"\" class=\"custom-file-download\">\n" +
    "          <img src=\"assets/images/download.png\"/><br/>Sample csv file\n" +
    "        </a>\n" +
    "      </div>\n" +
    "        \n" +
    "    </div>\n" +
    "  </div>\n" +
    "  \n" +
    "  </div>");
}]);

angular.module("siglerecord/singlerecord.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("siglerecord/singlerecord.tpl.html",
    "\n" +
    "<div class=\"row half-vertical-gutters align-middle\">\n" +
    "  <!-- <div class=\"column shrink\"><a id=\"mnav\"><i class=\"icon reorder\"></i></a></div> -->\n" +
    "  <div class=\"column\">\n" +
    "    <div class=\"wojo small breadcrumb\"> <div class=\"active section\">Data Upload</div> </div>\n" +
    "  </div>\n" +
    "  <div class=\"column shrink padding-right\">\n" +
    "    <div class=\"wojo top right pointing dropdown\" tabindex=\"0\"><img src=\"assets/images/blank.png\" alt=\"\" class=\"wojo basic tiny circular image\">\n" +
    "      <div class=\"menu\" tabindex=\"-1\"> <a class=\"item\" href=\"\"><!-- <i class=\"icon note\"></i> --> \n" +
    "      <i class=\"fa fa-pencil-square-o\" aria-hidden=\"true\"></i> My Account</a> \n" +
    "      <a class=\"item\" href=\"\"> <!-- <i class=\"icon lock\"></i> -->\n" +
    "      <i class=\"fa fa-unlock-alt\" aria-hidden=\"true\"></i> Change Password</a> </div>\n" +
    "    </div>\n" +
    "  </div>\n" +
    "    <div class=\"column shrink padding-right\">\n" +
    "    <div class=\"wojo top right pointing dropdown\" tabindex=\"0\">\n" +
    "      <i class=\"fa fa-bars\" aria-hidden=\"true\"></i>\n" +
    "      <div class=\"menu\" tabindex=\"-1\"> <a class=\"item\" href=\"\">Permissions</a> \n" +
    "      <a class=\"item\" href=\"\">Payment Options</a> \n" +
    "      <a class=\"item\" href=\"\">Transactions</a>\n" +
    "      <a class=\"item\" href=\"\">System</a>\n" +
    "      <a class=\"item\" href=\"\">Trash</a>\n" +
    "      </div>\n" +
    "    </div>\n" +
    "  </div>\n" +
    "    <div class=\"column shrink\"><a href=\"\">\n" +
    "    <i class=\"fa fa-power-off\" aria-hidden=\"true\"></i></a></div>\n" +
    "</div>\n" +
    "<h3>Single Record</h3>\n" +
    "<div class=\"wojo segment\">\n" +
    "  <div class=\"row gutters\">\n" +
    "    <div class=\"column \">\n" +
    "      <div class=\"singlerec\">\n" +
    "        <form>\n" +
    "          <label>Enter PAN Number</label>\n" +
    "          <div class=\"col-2\"><input type=\"Number\" name=\"enterpan\" placeholder=\"Enter Pan Number\" class=\"inputxt\"></div>\n" +
    "        </form>\n" +
    "      </div>\n" +
    "        \n" +
    "    </div>\n" +
    "  </div>\n" +
    "</div>\n" +
    "");
}]);

angular.module("singlerecord/singlerecord.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("singlerecord/singlerecord.tpl.html",
    "\n" +
    "<div class=\"row half-vertical-gutters align-middle\">\n" +
    "  <!-- <div class=\"column shrink\"><a id=\"mnav\"><i class=\"icon reorder\"></i></a></div> -->\n" +
    "  <div class=\"column\">\n" +
    "    <div class=\"wojo small breadcrumb\"> <div class=\"active section\">Data Upload</div> </div>\n" +
    "  </div>\n" +
    "  <div class=\"column shrink padding-right\">\n" +
    "    <div class=\"wojo top right pointing dropdown\" tabindex=\"0\"><img src=\"assets/images/blank.png\" alt=\"\" class=\"wojo basic tiny circular image\">\n" +
    "      <div class=\"menu\" tabindex=\"-1\"> <a class=\"item\" href=\"\"><!-- <i class=\"icon note\"></i> --> \n" +
    "      <i class=\"fa fa-pencil-square-o\" aria-hidden=\"true\"></i> My Account</a> \n" +
    "      <a class=\"item\" href=\"\"> <!-- <i class=\"icon lock\"></i> -->\n" +
    "      <i class=\"fa fa-unlock-alt\" aria-hidden=\"true\"></i> Change Password</a> </div>\n" +
    "    </div>\n" +
    "  </div>\n" +
    "    <div class=\"column shrink padding-right\">\n" +
    "    <div class=\"wojo top right pointing dropdown\" tabindex=\"0\">\n" +
    "      <i class=\"fa fa-bars\" aria-hidden=\"true\"></i>\n" +
    "      <div class=\"menu\" tabindex=\"-1\"> <a class=\"item\" href=\"\">Permissions</a> \n" +
    "      <a class=\"item\" href=\"\">Payment Options</a> \n" +
    "      <a class=\"item\" href=\"\">Transactions</a>\n" +
    "      <a class=\"item\" href=\"\">System</a>\n" +
    "      <a class=\"item\" href=\"\">Trash</a>\n" +
    "      </div>\n" +
    "    </div>\n" +
    "  </div>\n" +
    "    <div class=\"column shrink\"><a href=\"\">\n" +
    "    <i class=\"fa fa-power-off\" aria-hidden=\"true\"></i></a></div>\n" +
    "</div>\n" +
    "<h3>Single Record</h3>\n" +
    "<div class=\"wojo segment\">\n" +
    "  <div class=\"row gutters\">\n" +
    "    <div class=\"column \">\n" +
    "      <div class=\"singlerec\">\n" +
    "        <form>\n" +
    "          <label>Enter PAN Number</label>\n" +
    "          <div class=\"col-2\"><input type=\"Number\" name=\"enterpan\" placeholder=\"Enter Pan Number\" class=\"inputxt\"></div>\n" +
    "        </form>\n" +
    "      </div>\n" +
    "        \n" +
    "    </div>\n" +
    "  </div>\n" +
    "</div>\n" +
    "");
}]);

angular.module("uploaddocs/uploaddocs.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("uploaddocs/uploaddocs.tpl.html",
    "<h1>Hi we are here in the upload documents page</h1>\n" +
    "<div>\n" +
    "    <input id = \"file\" type=\"file\" file-model=\"myFile\"/>\n" +
    "    <button ng-click=\"uploadFile()\">upload me</button>\n" +
    "</div>\n" +
    "<div>\n" +
    "    <form method=\"get\" action=\"http://localhost:5000/downloadSample\">\n" +
    "        <input type=\"submit\" value=\"Download Sample\" />\n" +
    "    </form>\n" +
    "    <span style=\"color:red\">Please Make sure the coloumn names of the uploading files should be same as shown in the sample.</span>\n" +
    "</div>\n" +
    "<span>\n" +
    "  <div>\n" +
    "    <div class=\"gridStyle\" ng-grid=\"gridOptions\"></div>\n" +
    "  </div>\n" +
    "</span>\n" +
    "\n" +
    "");
}]);
